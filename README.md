# Setup Yarn Workspace

Yarn workspace is set up in the `frontend` folder in the Coscine folder / repo structure.

Lets assume you have all your coscine projects at the path `/home/dev/source/coscine/`.

## Initialize Workspace

Add files to a `/home/dev/source/coscine/frontend/` folder.

* `package.json` as provided
* `.yarnrc.yml` as provided
* `.yarn` Folder with files
  * `releases/yarn-berry.cjs`
  * `pluigns/plugin-workspace-lockfile.cjs`

see [yarnws.zip](yarnws.zip)

## Setup Typescript in VS Code

In `/home/dev/source/coscine/frontend/` execute:
```sh
> yarn dlx @yarnpkg/pnpify --sdk vscode
```

Add
```json
{
...
    "vetur.useWorkspaceDependencies": true
...
}
```
to you VSCode user settings.

## Clone Frontend Projects

In `/home/dev/source/coscine/frontend/` clone projects:

```sh
...
> git clone git@git.rwth-aachen.de:coscine/frontend/libraries/xxx libraries/xxx
> git clone git@git.rwth-aachen.de:coscine/frontend/apps/yyy apps/yyy
...
```

## Modify Workspace to Link Correct Projects

In `package.json` you can add projects that should be linked in the `workspaces` section.

If you modified `package.json` run `yarn install` in the root folder.

Post the workspace definition together with the branch names in the ticket and the merge request to make testing easier.
